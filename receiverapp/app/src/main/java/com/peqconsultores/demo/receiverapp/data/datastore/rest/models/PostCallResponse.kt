package com.peqconsultores.demo.receiverapp.data.datastore.rest.models
import com.google.gson.annotations.SerializedName


/**
 * Created by jesusabv93 on 29/04/2019.
 */
data class PostCallResponse(
    @SerializedName("error") val error: String,
    @SerializedName("insert") val insert: Int,
    @SerializedName("mensaje") val message: String
)