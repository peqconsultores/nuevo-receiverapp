package com.peqconsultores.demo.receiverapp.presentation.receiver

import android.content.Context
import com.peqconsultores.demo.receiverapp.BuildConfig.OriginPhone
import com.peqconsultores.demo.receiverapp.data.datastore.factory.CallFactory
import com.peqconsultores.demo.receiverapp.data.repository.implementations.CallRepositoryImpl
import com.peqconsultores.demo.receiverapp.domain.interactor.implementations.PostCallInteractorImpl
import com.peqconsultores.demo.receiverapp.presentation.models.CallReceiver
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.lang.Exception

/**
 * Created by jesusabv93 on 28/04/2019.
 */
class ReceiverPresenter(val context: Context, receiverView: ReceiverView) {

    private var view: ReceiverView? = null
    private val postCallInteractor = PostCallInteractorImpl(CallRepositoryImpl(CallFactory(context)))

    init {
        view = receiverView
    }

    fun onCreate(
        origin: String? = null,
        driverCode: String? = null,
        destinationPhone: String? = null,
        appOrigin: String? = null,
        versionAppOrigin: String? = null,
        order: String? = null,
        client: String? = null,
        terminal: String? = null,
        tel_origen_2: String?,
        oficina: String?,
        centro: String?
    ) {
        if (!(origin.isNullOrEmpty() || driverCode.isNullOrEmpty() || destinationPhone.isNullOrEmpty() || appOrigin.isNullOrEmpty() || versionAppOrigin.isNullOrEmpty() || order.isNullOrEmpty() || client.isNullOrEmpty() || terminal.isNullOrEmpty())) {
            val caller = CallReceiver(
                origin,
                driverCode,
                destinationPhone,
                appOrigin,
                versionAppOrigin,
                order,
                client,
                OriginPhone,
                terminal,
                tel_origen_2,
                oficina,
                centro
            )
            view?.setCall(caller)
            view?.setCallHistoryListener(caller)
        } else view?.setHideCaller()
    }

    fun onPermissionGranted(callReceiver: CallReceiver) {
        view?.goToCallActivity(callReceiver)
    }

    fun onDestroy() {
        view = null
    }

    fun createCall(callReceiver: CallReceiver, duration: Long) {
        try {
            GlobalScope.launch(Dispatchers.IO) {
                with(callReceiver) {
                    postCallInteractor.invoke(
                        appOrigin,
                        if (duration > 0) 1 else 0,
                        duration,
                        origin,
                        destinationPhone,
                        from,
                        versionAppOrigin,
                        order,
                        client,
                        terminal,
                        tel_origen_2,
                        oficina,
                        centro
                    )
                }
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

}