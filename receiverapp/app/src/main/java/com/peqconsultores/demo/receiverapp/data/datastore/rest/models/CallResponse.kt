package com.peqconsultores.demo.receiverapp.data.datastore.rest.models
import com.google.gson.annotations.SerializedName
import java.util.*

/**
 * Created by jesusabv93 on 29/04/2019.
 */
data class CallResponse(
    @SerializedName("app_origen") val appOrigin: String,
    @SerializedName("contesto") val answered: Int,
    @SerializedName("create_at") val createAt: Date,
    @SerializedName("duracion") val duration: Long,
    @SerializedName("id_call") val callId: Int,
    @SerializedName("origen") val origin: String,
    @SerializedName("tel_desitino") val destinationPhone: String,
    @SerializedName("tel_origen") val originPhone: String,
    @SerializedName("version_app_origen") val versionAppOrigin: String
)