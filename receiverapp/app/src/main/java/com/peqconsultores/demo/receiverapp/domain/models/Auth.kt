package com.peqconsultores.demo.receiverapp.domain.models

/**
 * Created by jesusabv93 on 18/04/2019.
 */
data class Auth(
    val token: String,
    val expirationData: String
)