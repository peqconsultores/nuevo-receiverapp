package com.peqconsultores.demo.receiverapp.data.datastore.rest.retrofit

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor.Level.BODY
import okhttp3.logging.HttpLoggingInterceptor.Level.NONE
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import com.peqconsultores.demo.receiverapp.BuildConfig.DomainApi
import com.peqconsultores.demo.receiverapp.BuildConfig.DEBUG
import okhttp3.logging.HttpLoggingInterceptor

/**
 * Created by jesusabv93 on 18/04/2019.
 */
object ApiProvider {
    val retrofit = Retrofit.Builder()
        .baseUrl(DomainApi)
        .addConverterFactory(GsonConverterFactory.create())
        .client(getClient()).build()

    val apiService = retrofit.create(ApiService::class.java)

    fun getClient() = OkHttpClient.Builder()
        .addInterceptor(HttpLoggingInterceptor().apply {
            level = if (DEBUG) BODY else NONE
        })
        .readTimeout(30000, TimeUnit.MILLISECONDS)
        .connectTimeout(30000, TimeUnit.MILLISECONDS)
        .writeTimeout(30000, TimeUnit.MILLISECONDS)
        .build()
}