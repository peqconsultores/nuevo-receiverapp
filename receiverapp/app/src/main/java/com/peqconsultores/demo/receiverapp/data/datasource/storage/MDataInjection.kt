package com.peqconsultores.demo.receiverapp.data.datasource.storage

import android.content.Context
import com.peqconsultores.demo.receiverapp.data.datasource.preferents.MDefaultSharedPref

object MDataInjection : MStorageInjection {
    override fun setUp(app: Context) {
        MDefaultSharedPref.instance.build(app)
    }

    override fun sharedPreferenceManager(): Any? {
        return  MDefaultSharedPref.instance
    }

    fun instance():MDataInjection{
        return this
    }

}