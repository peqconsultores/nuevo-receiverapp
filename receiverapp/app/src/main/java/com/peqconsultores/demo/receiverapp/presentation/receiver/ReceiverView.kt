package com.peqconsultores.demo.receiverapp.presentation.receiver

import com.peqconsultores.demo.receiverapp.presentation.models.CallReceiver

/**
 * Created by jesusabv93 on 28/04/2019.
 */
interface ReceiverView {
    fun setCallHistoryListener(callReceiver: CallReceiver)
    fun setHideCaller()
    fun setCall(callReceiver: CallReceiver)
    fun goToCallActivity(callReceiver: CallReceiver)
}