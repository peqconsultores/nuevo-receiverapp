package com.peqconsultores.demo.receiverapp.data.datastore.rest.models

/**
 * Created by jesusabv93 on 18/04/2019.
 */
class AuthRequest(
    val identity: String,
    val displayName: String
)