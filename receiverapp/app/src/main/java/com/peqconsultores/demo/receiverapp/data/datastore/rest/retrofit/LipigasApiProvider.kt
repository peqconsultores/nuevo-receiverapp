package com.peqconsultores.demo.receiverapp.data.datastore.rest.retrofit

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import com.peqconsultores.demo.receiverapp.BuildConfig.LipigasDomainApi
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import java.util.concurrent.TimeUnit

/**
 * Created by jesusabv93 on 29/04/2019.
 */
object LipigasApiProvider {

    var gson = GsonBuilder()
        .setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS")
        .create()

    val retrofit = Retrofit.Builder()
        .baseUrl(LipigasDomainApi)
        .addConverterFactory(GsonConverterFactory.create(gson))
        .client(
            OkHttpClient.Builder()
                .connectTimeout(10, TimeUnit.MINUTES)
            .writeTimeout(10, TimeUnit.MINUTES)
            .readTimeout(30, TimeUnit.MINUTES)
            .build()).build()

    val apiService = retrofit.create(LipigasApiService::class.java)

}