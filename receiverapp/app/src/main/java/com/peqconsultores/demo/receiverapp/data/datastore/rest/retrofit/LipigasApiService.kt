package com.peqconsultores.demo.receiverapp.data.datastore.rest.retrofit

import com.peqconsultores.demo.receiverapp.data.datastore.rest.models.CallHistoryResponse
import com.peqconsultores.demo.receiverapp.data.datastore.rest.models.PostCallResponse
import retrofit2.Call
import retrofit2.http.*

/**
 * Created by jesusabv93 on 29/04/2019.
 */
interface LipigasApiService {
    @GET("api/historial")
    fun getCalls(
        @Query("origen") origin: String
    ): Call<CallHistoryResponse>

    @FormUrlEncoded
    @POST("api/call_insert")
    fun postCall(
        @Field("app_origen") appOrigin: String,
        @Field("contesto") answered: Int,
        @Field("duracion") duration: Long,
        @Field("origen") origin: String,
        @Field("tel_destino") destinationPhone: String,
        @Field("tel_origen") originPhone: String,
        @Field("version_app_origen") versionAppOrigin: String,
        @Field("pedido") order: String,
        @Field("cliente") client: String,
        @Field("terminal") terminal: String,
        @Field("tel_origen_2") tel_origen_2: String?,
        @Field("oficina_sll") oficina: String?,
        @Field("centro") centro: String?
    ): Call<PostCallResponse>

}