package com.peqconsultores.demo.receiverapp.domain.interactor

import com.peqconsultores.demo.receiverapp.data.datastore.exception.CallException
import com.peqconsultores.demo.receiverapp.data.datastore.exception.NetworkException
import com.peqconsultores.demo.receiverapp.data.datastore.exception.ParseException

/**
 * Created by jesusabv93 on 01/05/2019.
 */
interface PostCallInteractor {
    @Throws(CallException::class, NetworkException::class, ParseException::class)
    suspend operator fun invoke(
        appOrigin: String,
        answered: Int,
        duration: Long,
        origin: String,
        destinationPhone: String,
        originPhone: String,
        versionAppOrigin: String,
        order: String,
        client: String,
        terminal: String,
        tel_origen_2: String?,
        oficina: String?,
        centro: String?
    ): Boolean
}