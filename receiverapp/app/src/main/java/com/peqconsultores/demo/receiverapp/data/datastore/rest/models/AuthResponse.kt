package com.peqconsultores.demo.receiverapp.data.datastore.rest.models

import com.google.gson.annotations.SerializedName


/**
 * Created by jesusabv93 on 18/04/2019.
 */
data class AuthResponse(
    @SerializedName("expirationTime") val expirationTime: String,
    @SerializedName("token") val token: String
)