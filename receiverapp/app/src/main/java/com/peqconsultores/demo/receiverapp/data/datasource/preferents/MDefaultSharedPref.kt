package com.peqconsultores.demo.receiverapp.data.datasource.preferents

import android.content.Context
import android.content.SharedPreferences

class MDefaultSharedPref : MSharedPreferences {
    private lateinit var context: Context
    private var appId = "marketing_personal_preferences"
    private object Holder { val INSTANCE = MDefaultSharedPref() }

    companion object {
        const val ORIGIN = "ORIGIN"
        const val DRIVER_CODE = "DRIVER_CODE"
        const val DESTINATION_PHONE = "DESTINATION_PHONE"
        const val DESTINATION_SECOND_PHONE = "DESTINATION_SECOND_PHONE"
        const val APP_ORIGIN = "APP_ORIGIN"
        const val VERSION_APP_ORIGIN = "VERSION_APP_ORIGIN"
        const val ORDER = "ORDER"
        const val CLIENT = "CLIENT"
        const val TERMINAL = "TERMINAL"
        const val OFICINA_SLL = "OFICINA_SLL"
        const val CENTRO = "CENTRO"
        const val FLAG = "FLAG"

        val instance: MDefaultSharedPref by lazy { Holder.INSTANCE }
    }

    override fun build(contxt: Context){
        context= contxt
    }

    override fun clearSession() {
        getEditor(context)
            .clear()
            .apply()
    }

    private fun getEditor(context: Context): SharedPreferences.Editor {
        val preferences = getSharedPreferences(context)
        return preferences.edit()
    }

    private fun getSharedPreferences(context: Context): SharedPreferences {
        return context.getSharedPreferences(appId, Context.MODE_PRIVATE)
    }

    private fun keyOrigin():String{
        return appId+ ORIGIN
    }
    private fun keyDestinationPhone():String{
        return appId+ DESTINATION_PHONE
    }
    private fun keyDriverCode():String{
        return appId+ DRIVER_CODE
    }
    private fun keyAppOrigin():String{
        return appId+ APP_ORIGIN
    }
    private fun keyVersionAppOrigin():String{
        return appId+ VERSION_APP_ORIGIN
    }
    private fun keyDestinationSecondPhone():String{
        return appId+ DESTINATION_SECOND_PHONE
    }
    private fun keyOrder():String{
        return appId+ ORDER
    }
    private fun keyClient():String{
        return appId+ CLIENT
    }
    private fun keyTerminal():String{
        return appId+ TERMINAL
    }
    private fun keyOficinaSLL():String{
        return appId+ OFICINA_SLL
    }
    private fun keyCntro():String{
        return appId+ CENTRO
    }
    private fun keyFlag():String{
        return appId+ FLAG
    }

    override fun saveOrigin(origin: String) {
        getEditor(context).putString(keyOrigin(),origin).apply()
    }

    override fun saveDriverCode(driverCode: String) {
        getEditor(context).putString(keyDriverCode(),driverCode).apply()
    }

    override fun saveDestinationPhone(destinationPhone: String) {
        getEditor(context).putString(keyDestinationPhone(),destinationPhone).apply()
    }

    override fun saveSecondDestinationPhone(secondDestinationPhone: String) {
        getEditor(context).putString(keyDestinationSecondPhone(),secondDestinationPhone).apply()
    }

    override fun saveAppOrigin(appOrigin: String) {
        getEditor(context).putString(keyAppOrigin(),appOrigin).apply()
    }

    override fun saveVersionAppOrigin(word: String) {
        getEditor(context).putString(keyVersionAppOrigin(),word).apply()
    }

    override fun saveOrder(word: String) {
        getEditor(context).putString(keyOrder(),word).apply()
    }

    override fun saveClient(client: String) {
        getEditor(context).putString(keyClient(),client).apply()
    }

    override fun saveTerminal(terminal: String) {
        getEditor(context).putString(keyTerminal(),terminal).apply()
    }

    override fun saveOficinaSLL(oficinaSLL: String) {
        getEditor(context).putString(keyOficinaSLL(),oficinaSLL).apply()
    }

    override fun saveCentro(centro: String) {
        getEditor(context).putString(keyCntro(),centro).apply()
    }

    override fun saveFlag(flag: String) {
        getEditor(context).putString(keyFlag(),flag).apply()
    }

    override fun getOrigin(): String {
        return getSharedPreferences(context).getString(keyOrigin(),"")?:""
    }

    override fun getDriverCode(): String {
        return getSharedPreferences(context).getString(keyDriverCode(),"")?:""
    }

    override fun getDestinationPhone(): String {
        return getSharedPreferences(context).getString(keyDestinationPhone(),"")?:""
    }

    override fun getSecondDestinationPhone(): String {
        return getSharedPreferences(context).getString(keyDestinationSecondPhone(),"")?:""
    }

    override fun getAppOrigin(): String {
        return getSharedPreferences(context).getString(keyAppOrigin(),"")?:""
    }

    override fun getVersionAppOrigin(): String {
        return getSharedPreferences(context).getString(keyVersionAppOrigin(),"")?:""
    }

    override fun getOrder(): String {
        return getSharedPreferences(context).getString(keyOrder(),"")?:""
    }

    override fun getClient(): String {
        return getSharedPreferences(context).getString(keyClient(),"")?:""
    }

    override fun getTerminal(): String {
        return getSharedPreferences(context).getString(keyTerminal(),"")?:""
    }

    override fun getOficinaSLL(): String {
        return getSharedPreferences(context).getString(keyOficinaSLL(),"")?:""
    }

    override fun getCentro(): String {
        return getSharedPreferences(context).getString(keyCntro(),"")?:""
    }

    override fun getFlag(): String {
        return getSharedPreferences(context).getString(keyFlag(),"")?:""
    }
}