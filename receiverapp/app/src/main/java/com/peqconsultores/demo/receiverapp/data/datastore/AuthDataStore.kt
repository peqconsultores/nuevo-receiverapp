package com.peqconsultores.demo.receiverapp.data.datastore

import com.peqconsultores.demo.receiverapp.data.datastore.exception.AuthException
import com.peqconsultores.demo.receiverapp.data.datastore.exception.NetworkException
import com.peqconsultores.demo.receiverapp.data.datastore.exception.ParseException
import com.peqconsultores.demo.receiverapp.data.datastore.rest.models.AuthResponse

/**
 * Created by jesusabv93 on 18/04/2019.
 */
interface AuthDataStore {
    @Throws(AuthException::class, NetworkException::class, ParseException::class)
    fun auth(identity: String, displayName: String): AuthResponse
}