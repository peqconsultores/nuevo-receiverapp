package com.peqconsultores.demo.receiverapp.presentation.receiver

//import com.infobip.webrtc.sdk.api.RTCOptions
//import com.infobip.webrtc.sdk.api.event.RTCEventListener
//import com.infobip.webrtc.sdk.api.event.rtc.ConnectedEvent
//import com.infobip.webrtc.sdk.api.event.rtc.DisconnectedEvent
//import com.infobip.webrtc.sdk.api.event.rtc.ReconnectedEvent
//import com.infobip.webrtc.sdk.api.event.rtc.ReconnectingEvent

import android.content.Context
import android.util.Log
import com.google.firebase.crashlytics.FirebaseCrashlytics
import com.infobip.webrtc.sdk.api.InfobipRTC
import com.infobip.webrtc.sdk.api.call.PhoneCall
import com.infobip.webrtc.sdk.api.event.call.CallEarlyMediaEvent
import com.infobip.webrtc.sdk.api.event.call.CallEstablishedEvent
import com.infobip.webrtc.sdk.api.event.call.CallHangupEvent
import com.infobip.webrtc.sdk.api.event.call.CallRingingEvent
import com.infobip.webrtc.sdk.api.event.call.ErrorEvent
import com.infobip.webrtc.sdk.api.event.listener.PhoneCallEventListener
import com.infobip.webrtc.sdk.api.options.PhoneCallOptions
import com.infobip.webrtc.sdk.api.request.CallPhoneRequest
import com.peqconsultores.demo.receiverapp.data.datastore.factory.AuthFactory
import com.peqconsultores.demo.receiverapp.data.datastore.rest.mappers.AuthMapper
import com.peqconsultores.demo.receiverapp.data.repository.implementations.AuthRepositoryImpl
import com.peqconsultores.demo.receiverapp.domain.interactor.implementations.AuthInteractorImpl
import com.peqconsultores.demo.receiverapp.presentation.models.CallReceiver
import com.peqconsultores.demo.receiverapp.presentation.receiver.views.CallView
import com.peqconsultores.demo.receiverapp.presentation.utils.Scope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


/**
 * Created by jesusabv93 on 18/04/2019.
 */
class CallPresenter(val context: Context, callView: CallView) : Scope by Scope.Impl() {

    companion object {
        val TAG: String = CallPresenter::class.java.simpleName
    }

    private var view: CallView? = null
    private var infobipRTC: InfobipRTC? = null
    private var phoneCall: PhoneCall? = null

    init {
        view = callView
    }

    private val authInteractor = AuthInteractorImpl(AuthRepositoryImpl(AuthFactory(context)).apply {
        authMapper = AuthMapper()
    })

    fun onCreate(callReceiver: CallReceiver?) {
        view?.setEnableViews(false)
        initScope()
        if (callReceiver == null) return
        view?.setCaller(callReceiver)
        view?.setListeners()
        call(callReceiver)
    }

    private fun call(callReceiver: CallReceiver) = launch(Dispatchers.IO)  {

        val token = try {
            authInteractor.invoke(callReceiver.client, callReceiver.client).token
        } catch (ex: Exception) {
            ex.printStackTrace()
            null
        } ?: return@launch

        try{
            val infobipRTC = InfobipRTC.getInstance()

            val callPhoneRequest = CallPhoneRequest(
                token,
                context,
              callReceiver.destinationPhone,
                callEventListener(callReceiver)
            )
            val phoneCallOptions = PhoneCallOptions.builder().from(callReceiver.from).build()

            phoneCall = infobipRTC.callPhone(callPhoneRequest, phoneCallOptions)
        }catch (ex: Exception) {
            ex.printStackTrace()

        }
    }

    private fun callEventListener(callReceiver: CallReceiver): PhoneCallEventListener {

        return object : PhoneCallEventListener {
            /* override fun onHangup(callHangupEvent: CallHangupEvent?) {
                view?.onHangup(callReceiver)
                view?.setEnableViews(false)
                view?.stopChronometer()
                view?.setState("Llamada finalizada", callHangupEvent?.errorCode?.id.toString(), callReceiver)

                Log.e("Error code", callHangupEvent?.errorCode?.id.toString())

            }

            override fun onEstablished(callEstablishedEvent: CallEstablishedEvent?) {
                view?.setState("Conectando llamada","", callReceiver)
                view?.setEnableViews(true)
            }

            override fun onEarlyMedia(callEarlyMediaEvent: CallEarlyMediaEvent?) {
                view?.setEnableViews(true)

            }

            override fun onUpdated(callUpdatedEvent: CallUpdatedEvent?) {
               - view?.setEnableViews(true)

            }

            override fun onError(callErrorEvent: CallErrorEvent?) {
                //Crashlog(DEBUG, TAG, "callErrorEvent: ${callErrorEvent?.reason}")
                //Firebase.crashlytics
                FirebaseCrashlytics.getInstance().log("callErrorEvent: ${callErrorEvent?.reason}")

                view?.setState("Error conectando llamada","",callReceiver)
                view?.setEnableViews(false)
            }
            override fun onRinging(callRingingEvent: CallRingingEvent?) {
                view?.setState("Llamada conectada", "", callReceiver)
                view?.setEnableViews(true)
                view?.startChronometer()
            }*/
            override fun onRinging(callRingingEvent: CallRingingEvent?) {
                view?.setState("Llamada conectada", "", callReceiver)
                view?.setEnableViews(true)
                view?.startChronometer()
            }

            override fun onEarlyMedia(callEarlyMediaEvent: CallEarlyMediaEvent?) {
                view?.setEnableViews(true)
            }

            override fun onEstablished(callEstablishedEvent: CallEstablishedEvent?) {
                view?.setState("Llamada conectada", "", callReceiver)
                view?.setEnableViews(true)
                view?.startChronometer()
            }

            override fun onHangup(callHangupEvent: CallHangupEvent?) {
                view?.onHangup(callReceiver)
                view?.setEnableViews(false)
                view?.stopChronometer()
                view?.setState("Llamada finalizada", callHangupEvent?.errorCode?.id.toString(), callReceiver)

            }

            override fun onError(errorEvent: ErrorEvent?) {
                //Crashlog(DEBUG, TAG, "callErrorEvent: ${callErrorEvent?.reason}")
                //Firebase.crashlytics
                FirebaseCrashlytics.getInstance().log("callErrorEvent: ${errorEvent?.errorCode?.description}")

                view?.setState("Error conectando llamada","",callReceiver)
                view?.setEnableViews(false)
            }


        }
    }


    /*private fun call(callReceiver: CallReceiver) = launch(Dispatchers.IO) {
        val token = try {
            authInteractor.invoke(callReceiver.client, callReceiver.client)
        } catch (ex: Exception) {
            ex.printStackTrace()
            null
        } ?: return@launch

        infobipRTC =  DefaultInfobipRTC.getInstance(token.token, RTCOptions.builder().debug(true).build(), context)
       infobipRTC?.addEventListener(object : RTCEventListener {
            override fun onReconnected(reconnectedEvent: ReconnectedEvent?) {
                log(DEBUG, TAG, "onReconnected: ${reconnectedEvent?.toString()}")
            }

            override fun onConnected(connectedEvent: ConnectedEvent?) {
                view?.setState("Listo para conectar")
                outgoingCall = infobipRTC?.callPhoneNumber(
                    callReceiver.destinationPhone,
                    CallPhoneNumberOptions.builder().from(callReceiver.from).build()
                )
                outgoingCall?.addEventListener(object : CallEventListener {
                    override fun onHangup(callHangupEvent: CallHangupEvent?) {
                        view?.setState("Llama finalizada")
                        view?.setEnableViews(false)
                        view?.stopChronometer()
                        view?.onHangup(callReceiver)
                    }

                    override fun onEstablished(callEstablishedEvent: CallEstablishedEvent?) {
                        view?.setState("Llamada conectada")
                        view?.setEnableViews(true)
                        view?.startChronometer()
                    }

                    override fun onError(callErrorEvent: CallErrorEvent?) {
                        log(DEBUG, TAG, "callErrorEvent: ${callErrorEvent?.reason}")
                        view?.setState("Error conectando llamada")
                        view?.setEnableViews(false)
                    }

                    override fun onRinging(callRingingEvent: CallRingingEvent?) {
                        view?.setState("Conectando llamada")
                        view?.setEnableViews(false)
                    }
                })
            }

            override fun onDisconnected(disconnectedEvent: DisconnectedEvent?) {
                log(DEBUG, TAG, "disconnectedEvent: ${disconnectedEvent?.reason}")
            }

            override fun onReconnecting(reconnectingEvent: ReconnectingEvent?) {
                log(DEBUG, TAG, "onReconnecting: ${reconnectingEvent?.toString()}")
            }
        })
        infobipRTC?.connect()
    }*/

    fun mute(shouldMute: Boolean) {
        phoneCall?.mute(shouldMute)
        view?.setDrawableMute(!shouldMute)
    }

    fun hangup() {
        phoneCall?.hangup()
    }

    fun speakerPhone(enabled: Boolean){
        phoneCall?.speakerphone(enabled)
        view?.setDrawableSpekerPhone(!enabled)
    }

    fun onDestroy() {
        //infobipRTC?.disconnect()
        view = null
        cancelScope()
    }

}