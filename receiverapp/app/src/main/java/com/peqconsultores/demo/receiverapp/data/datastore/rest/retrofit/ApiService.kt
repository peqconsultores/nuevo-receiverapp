package com.peqconsultores.demo.receiverapp.data.datastore.rest.retrofit

import com.peqconsultores.demo.receiverapp.data.datastore.rest.models.AuthRequest
import com.peqconsultores.demo.receiverapp.data.datastore.rest.models.AuthResponse
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.Header
import retrofit2.http.POST

/**
 * Created by jesusabv93 on 18/04/2019.
 */
interface ApiService {

    @POST("webrtc/1/token")
    fun auth(
        @Header("Authorization") authorization: String,
        @Body request: AuthRequest
    ): Call<AuthResponse>

}