package com.peqconsultores.demo.receiverapp.data.datasource.storage

import android.content.Context

interface MStorageInjection {
    fun setUp(app: Context)
    fun sharedPreferenceManager():Any?
}