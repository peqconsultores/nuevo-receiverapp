package com.peqconsultores.demo.receiverapp.data.datasource.preferents

import android.content.Context

interface MSharedPreferences {
    fun build(context: Context)
    fun clearSession()

    fun saveOrigin(origin: String)
    fun saveDriverCode(driverCode: String)
    fun saveDestinationPhone(destinationPhone: String)
    fun saveSecondDestinationPhone(secondDestinationPhone: String)
    fun saveAppOrigin(appOrigin: String)
    fun saveVersionAppOrigin(word: String)
    fun saveOrder(word: String)
    fun saveClient(client: String)
    fun saveTerminal(terminal: String)
    fun saveOficinaSLL(oficinaSLL: String)
    fun saveCentro(centro: String)
    fun saveFlag(flag: String)

    fun getOrigin():String
    fun getDriverCode():String
    fun getDestinationPhone():String
    fun getSecondDestinationPhone():String
    fun getAppOrigin():String
    fun getVersionAppOrigin():String
    fun getOrder():String
    fun getClient():String
    fun getTerminal():String
    fun getOficinaSLL():String
    fun getCentro():String
    fun getFlag():String
}