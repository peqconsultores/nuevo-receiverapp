package com.peqconsultores.demo.receiverapp.presentation.callhistory.views.activities

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.View.GONE
import androidx.recyclerview.widget.LinearLayoutManager
import com.peqconsultores.demo.receiverapp.R
import com.peqconsultores.demo.receiverapp.domain.models.Call
import com.peqconsultores.demo.receiverapp.presentation.callhistory.CallHistoryPresenter
import com.peqconsultores.demo.receiverapp.presentation.callhistory.views.CallHistoryView
import com.peqconsultores.demo.receiverapp.presentation.callhistory.views.adapters.CallAdapter
import com.peqconsultores.demo.receiverapp.presentation.models.CallReceiver
import kotlinx.android.synthetic.main.activity_calls_history.*

class CallsHistoryActivity : AppCompatActivity(), CallHistoryView {

    companion object {
        val CALL_RECEIVER = "callReceiver"
    }

    private lateinit var handler: Handler

    private var callHistoryPresenter: CallHistoryPresenter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_calls_history)
        handler = Handler()
        callHistoryPresenter = CallHistoryPresenter(this as Context, this)
        callHistoryPresenter?.onCreate(intent?.extras?.getParcelable(CALL_RECEIVER) as? CallReceiver)
    }

    override fun setupRecyclerView() {
        rvCalls.setHasFixedSize(true)
        rvCalls.layoutManager = LinearLayoutManager(this)
    }

    override fun updateCalls(calls: List<Call>) {
        handler.post {
            rvCalls.adapter = CallAdapter(calls)
        }
    }

    override fun hideProgress() {
        handler.post {
            pbLoading.visibility = GONE
        }
    }

    override fun onDestroy() {
        callHistoryPresenter?.onDestroy()
        super.onDestroy()
    }
}
