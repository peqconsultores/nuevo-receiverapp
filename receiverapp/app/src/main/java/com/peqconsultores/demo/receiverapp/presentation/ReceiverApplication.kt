package com.peqconsultores.demo.receiverapp.presentation

import android.app.Application
import com.peqconsultores.demo.receiverapp.data.datasource.storage.MDataInjection
import com.peqconsultores.demo.receiverapp.data.datasource.storage.MStorageInjection

class ReceiverApplication : Application() {
    lateinit var mStorageInjection: MStorageInjection

    override fun onCreate() {
        super.onCreate()

        mStorageInjection=  MDataInjection.instance()
        mStorageInjection.setUp(this)
    }
}