package com.peqconsultores.demo.receiverapp.data.datastore.rest.mappers

import com.peqconsultores.demo.receiverapp.data.datastore.Mapper
import com.peqconsultores.demo.receiverapp.data.datastore.rest.models.AuthResponse
import com.peqconsultores.demo.receiverapp.domain.models.Auth

/**
 * Created by jesusabv93 on 18/04/2019.
 */
class AuthMapper : Mapper<AuthResponse, Auth>() {
    override fun map(value: AuthResponse) = Auth(
        token = value.token,
        expirationData = value.expirationTime
    )
}