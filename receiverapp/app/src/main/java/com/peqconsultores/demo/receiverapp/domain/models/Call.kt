package com.peqconsultores.demo.receiverapp.domain.models

import java.util.*

/**
 * Created by jesusabv93 on 29/04/2019.
 */
data class Call(
    val appOrigin: String,
    val answered: Boolean,
    val createAt: Date,
    val duration: Long,
    val callId: Int,
    val origin: String,
    val destinationPhone: String,
    val originPhone: String,
    val versionAppOrigin: String
)