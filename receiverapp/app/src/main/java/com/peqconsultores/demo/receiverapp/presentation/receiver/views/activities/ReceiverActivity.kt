package com.peqconsultores.demo.receiverapp.presentation.receiver.views.activities

import android.Manifest.permission.*
import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Parcelable
import android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS
import android.util.Log
import android.view.View.GONE
import android.view.View.OnClickListener
import android.view.View.VISIBLE
import android.view.Window
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.PermissionChecker.PERMISSION_GRANTED
import androidx.core.text.bold
import androidx.core.text.buildSpannedString
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.snackbar.Snackbar.*
import com.karumi.dexter.Dexter
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.single.PermissionListener
import com.peqconsultores.demo.receiverapp.R
import com.peqconsultores.demo.receiverapp.data.datasource.preferents.MAccessManager
import com.peqconsultores.demo.receiverapp.data.datasource.preferents.MDefaultSharedPref
import com.peqconsultores.demo.receiverapp.data.datasource.storage.MDataInjection
import com.peqconsultores.demo.receiverapp.presentation.models.CallReceiver
import com.peqconsultores.demo.receiverapp.presentation.receiver.ReceiverPresenter
import com.peqconsultores.demo.receiverapp.presentation.receiver.ReceiverView
import com.peqconsultores.demo.receiverapp.presentation.receiver.views.activities.CallActivity.Companion.CALL_RECEIVER
import com.peqconsultores.demo.receiverapp.presentation.receiver.views.activities.CallActivity.Companion.DURATION
import kotlinx.android.synthetic.main.activity_receiver.*


class ReceiverActivity : AppCompatActivity(), ReceiverView {

    companion object {
        val ORIGIN = "origin"
        val DRIVER_CODE = "driver_code"
        val DESTINATION_PHONE = "destination_phone"
        val DESTINATION_SECOND_PHONE = "destination_second_phone"
        val APP_ORIGIN = "app_origin"
        val VERSION_APP_ORIGIN = "version_app_origin"
        val ORDER = "order"
        val CLIENT = "client"
        val TERMINAL = "terminal"
        val CALL_REQUEST_CODE = 100
        val OFICINA_SLL = "oficina_sll"
        val CENTRO = "centro"
        val FLAG = "flag"
    }

    private var snackBar: Snackbar? = null
    private var receiverPresenter: ReceiverPresenter? = null
    var llamarSegundoNumero = ""
    var dialog : Dialog? = null
    var callReceiverThis: CallReceiver? = null
    val sp: MDefaultSharedPref =  MDataInjection.instance().sharedPreferenceManager() as MDefaultSharedPref

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_receiver)
        requestPermissions()

        getExtras()

        intent.data?.getQueryParameter(ORIGIN)?.let { MAccessManager.saveOrigin(sp, it) }
        intent.data?.getQueryParameter(DRIVER_CODE)?.let { MAccessManager.saveDriverCode(sp, it) }
        intent.data?.getQueryParameter(DESTINATION_PHONE)
            ?.let { MAccessManager.saveDestinationPhone(sp, it) }
        intent.data?.getQueryParameter(DESTINATION_SECOND_PHONE)
            ?.let { MAccessManager.saveSecondDestinationPhone(sp, it) }
        intent.data?.getQueryParameter(APP_ORIGIN)?.let { MAccessManager.saveAppOrigin(sp, it) }
        intent.data?.getQueryParameter(VERSION_APP_ORIGIN)
            ?.let { MAccessManager.saveVersionAppOrigin(sp, it) }
        intent.data?.getQueryParameter(ORDER)?.let { MAccessManager.saveOrder(sp, it) }
        intent.data?.getQueryParameter(CLIENT)?.let { MAccessManager.saveClient(sp, it) }
        intent.data?.getQueryParameter(TERMINAL)?.let { MAccessManager.saveTerminal(sp, it) }
        intent.data?.getQueryParameter(OFICINA_SLL)?.let { MAccessManager.saveOficinaSLL(sp, it) }
        intent.data?.getQueryParameter(CENTRO)?.let { MAccessManager.saveCentro(sp, it) }
        intent.data?.getQueryParameter(FLAG)?.let { MAccessManager.saveFlag(sp, it) }

        Log.e("ORIGIN", sp.getOrigin())
        Log.e("DESTINATION_PHONE", sp.getDestinationPhone())
        Log.e("DESTINATION_SEC", sp.getSecondDestinationPhone())
        Log.e("FLAG", sp.getFlag())
        Log.e("OFICINA_SLL", sp.getOficinaSLL())
        Log.e("CENTRO", sp.getCentro())

        if (llamarSegundoNumero == "1") {
            showMessage()
        } else {
            receiverPresenter = ReceiverPresenter(this as Context, this)
            receiverPresenter?.onCreate(
                sp.getOrigin(),
                sp.getDriverCode(),
                sp.getDestinationPhone(),
                sp.getAppOrigin(),
                sp.getVersionAppOrigin(),
                sp.getOrder(),
                sp.getClient(),
                sp.getTerminal(),
                sp.getSecondDestinationPhone(),
                sp.getOficinaSLL(),
                sp.getCentro()
            )
        }

       /* receiverPresenter = ReceiverPresenter(this as Context, this)
        receiverPresenter?.onCreate(
            intent.data?.getQueryParameter(ORIGIN),
            intent.data?.getQueryParameter(DRIVER_CODE),
            intent.data?.getQueryParameter(DESTINATION_PHONE),
            intent.data?.getQueryParameter(APP_ORIGIN),
            intent.data?.getQueryParameter(VERSION_APP_ORIGIN),
            intent.data?.getQueryParameter(ORDER),
            intent.data?.getQueryParameter(CLIENT),
            intent.data?.getQueryParameter(TERMINAL)
        )*/
    }

    private fun getExtras() {
        var llamarSegundoNumeroOne = intent.getSerializableExtra("llamar_segundo_numero")
        var call_centerOne = intent?.extras?.getParcelable<Parcelable>("call_center")

        if(llamarSegundoNumeroOne != null && llamarSegundoNumeroOne != "") {
            llamarSegundoNumero = llamarSegundoNumeroOne as String
        }

        if(call_centerOne != null ) {
            callReceiverThis = call_centerOne as CallReceiver
        }
    }

    fun showMessage() {
        dialog = Dialog(this)
        dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog?.setCancelable(false)
        dialog?.setContentView(R.layout.segunda_llamada_dialog)

        val btnAceptar = dialog?.findViewById(R.id.btnAceptar) as Button
        val tvMessage = dialog?.findViewById(R.id.tvMessage) as TextView
        val btnCancelar = dialog?.findViewById(R.id.btnCancelar) as Button
        tvMessage.text = "¿Desea realizar una segunda llamada?"

        if (callReceiverThis != null) {
            btnAceptar.tag = callReceiverThis
        }

        btnAceptar.setOnClickListener {
            dialog!!.dismiss()

            if(llamarSegundoNumero == "1") callReceiverThis?.destinationPhone = sp.getSecondDestinationPhone()
            Log.e("destinationPhone", callReceiverThis?.destinationPhone.toString())

            startActivityForResult(Intent(this, CallActivity::class.java).apply {
                putExtra(CALL_RECEIVER, callReceiverThis)
            }, CALL_REQUEST_CODE)
        }

        btnCancelar.setOnClickListener {
            sp.clearSession()
            dialog!!.dismiss()

        }

        dialog!!.show()
    }
    override fun setCallHistoryListener(callReceiver: CallReceiver) {
       /* btnCallsHistory.setOnClickListener {
            startActivity<CallsHistoryActivity> {
                putExtra(
                    CallsHistoryActivity.CALL_RECEIVER,
                    callReceiver
                )
            }
        }*/
    }

    override fun setHideCaller() {
        tvData.text = ""
        tvData.visibility = VISIBLE
        btnCall.visibility = GONE
    }

    override fun setCall(callReceiver: CallReceiver) {
        tvData.text = buildSpannedString {
            bold { append("Origen: ") }
            appendln(callReceiver.origin)
            appendln("")

            bold { append("Código de Chofer: ") }
            appendln(callReceiver.driverCode)
            appendln("")

            bold { append("Teléfono de Cliente: ") }
            appendln(getString(R.string.row_call_call_label_phone, callReceiver.destinationPhone.takeLast(4)))
            appendln("")

            bold { append("Aplicación origen: ") }
            appendln(callReceiver.appOrigin)
            appendln("")

            bold { append("Versión aplicación origen: ") }
            appendln(callReceiver.versionAppOrigin)
            appendln("")

            bold { append("Pedido: ") }
            appendln(callReceiver.order)
            appendln("")

            bold { append("Cliente: ") }
            appendln(callReceiver.client)
            appendln("")

            bold { append("Terminal: ") }
            appendln(callReceiver.terminal)
        }
        btnCall.tag = callReceiver
        btnCall.setOnClickListener(callOnClickListener)
        tvData.visibility = VISIBLE
        btnCall.visibility = VISIBLE
    }

    private val callOnClickListener = OnClickListener {
        Dexter.withActivity(this)
            .withPermission(RECORD_AUDIO)
            .withListener(object : PermissionListener {
                override fun onPermissionGranted(response: PermissionGrantedResponse?) {
                    receiverPresenter?.onPermissionGranted(it.tag as CallReceiver)
                }

                override fun onPermissionRationaleShouldBeShown(
                    permission: PermissionRequest?,
                    token: PermissionToken?
                ) {
                    token?.continuePermissionRequest()
                }

                override fun onPermissionDenied(response: PermissionDeniedResponse?) {
                    showSnackBar()
                }
            })
            .check()
    }

    fun showSnackBar() {
        snackBar =
            make(rootView, R.string.permission_snackbar_message, LENGTH_LONG)
                .setAction(R.string.permission_snackbar_action) {
                    startActivity(
                        Intent(
                            ACTION_APPLICATION_DETAILS_SETTINGS,
                            Uri.fromParts("package", packageName, null)
                        ).apply {
                            addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                        })
                }
        snackBar?.show()
    }

    override fun goToCallActivity(callReceiver: CallReceiver) {
        startActivityForResult(Intent(this, CallActivity::class.java).apply {
            putExtra(CALL_RECEIVER, callReceiver)
        }, CALL_REQUEST_CODE)
    }

    override fun onDestroy() {
        receiverPresenter?.onDestroy()
        super.onDestroy()
    }
    private fun requestPermissions() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
            if (permissionNotGranted(BLUETOOTH_CONNECT) || permissionNotGranted(RECORD_AUDIO)
            ) {
                ActivityCompat.requestPermissions(
                    this, arrayOf(
                        RECORD_AUDIO,
                        BLUETOOTH_CONNECT
                    ), 200
                )
            }
        } else {
            if (permissionNotGranted(RECORD_AUDIO)) {
                ActivityCompat.requestPermissions(
                    this, arrayOf(
                        RECORD_AUDIO
                    ), 200
                )
            }
        }
    }

    private fun permissionNotGranted(permission: String): Boolean {
        return ContextCompat.checkSelfPermission(this, permission) != PERMISSION_GRANTED
    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == CALL_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                receiverPresenter?.createCall(
                    data?.getParcelableExtra(CALL_RECEIVER)!!,
                    data?.getLongExtra(DURATION, 0L)!!
                )
            }
        }

    }

}