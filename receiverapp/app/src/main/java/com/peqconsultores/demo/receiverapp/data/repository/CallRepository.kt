package com.peqconsultores.demo.receiverapp.data.repository

import com.peqconsultores.demo.receiverapp.data.datastore.exception.CallException
import com.peqconsultores.demo.receiverapp.data.datastore.exception.NetworkException
import com.peqconsultores.demo.receiverapp.data.datastore.exception.ParseException
import com.peqconsultores.demo.receiverapp.domain.models.Call

/**
 * Created by jesusabv93 on 29/04/2019.
 */
interface CallRepository {
    @Throws(CallException::class, NetworkException::class, ParseException::class)
    fun getCalls(origin: String): List<Call>

    @Throws(CallException::class, NetworkException::class, ParseException::class)
    fun postCall(
        appOrigin: String,
        answered: Int,
        duration: Long,
        origin: String,
        destinationPhone: String,
        originPhone: String,
        versionAppOrigin: String,
        order: String,
        client: String,
        terminal: String,
        tel_origen_2: String?,
        oficina: String?,
        centro: String?
    ): Boolean
}