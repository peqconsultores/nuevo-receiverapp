package com.peqconsultores.demo.receiverapp.data.repository.implementations

import com.peqconsultores.demo.receiverapp.data.datastore.factory.AuthFactory
import com.peqconsultores.demo.receiverapp.data.datastore.factory.DataStoreEnum.REST
import com.peqconsultores.demo.receiverapp.data.datastore.rest.mappers.AuthMapper
import com.peqconsultores.demo.receiverapp.data.repository.AuthRepository
import com.peqconsultores.demo.receiverapp.domain.models.Auth

/**
 * Created by jesusabv93 on 18/04/2019.
 */
class AuthRepositoryImpl(val factory: AuthFactory) : AuthRepository {
    lateinit var authMapper: AuthMapper

    override fun auth(identity: String, displayName: String): Auth {
        return authMapper.map(factory.create(REST).auth(identity, displayName))
    }
}