package com.peqconsultores.demo.receiverapp.presentation.callhistory

import android.content.Context
import com.peqconsultores.demo.receiverapp.data.datastore.factory.CallFactory
import com.peqconsultores.demo.receiverapp.data.datastore.rest.mappers.CallMapper
import com.peqconsultores.demo.receiverapp.data.repository.implementations.CallRepositoryImpl
import com.peqconsultores.demo.receiverapp.domain.interactor.implementations.GetCallsInteractorImpl
import com.peqconsultores.demo.receiverapp.presentation.callhistory.views.CallHistoryView
import com.peqconsultores.demo.receiverapp.presentation.models.CallReceiver
import com.peqconsultores.demo.receiverapp.presentation.utils.Scope
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch
import java.lang.Exception

/**
 * Created by jesusabv93 on 29/04/2019.
 */
class CallHistoryPresenter(val context: Context, callHistoryView: CallHistoryView) : Scope by Scope.Impl() {

    private var view: CallHistoryView? = null
    private val getCallsInteractor = GetCallsInteractorImpl(CallRepositoryImpl(CallFactory(context)).apply {
        callMapper = CallMapper()
    })

    init {
        view = callHistoryView
    }

    fun onCreate(callReceiver: CallReceiver?) {
        if (callReceiver == null) return
        initScope()
        view?.setupRecyclerView()
        launch(IO) {
            try {
                val calls = getCallsInteractor.invoke(callReceiver.origin)
                view?.updateCalls(calls)
                view?.hideProgress()
            } catch (ex: Exception) {
                ex.printStackTrace()
                view?.updateCalls(listOf())
            }
        }
    }

    fun onDestroy() {
        view = null
        cancelScope()
    }

}