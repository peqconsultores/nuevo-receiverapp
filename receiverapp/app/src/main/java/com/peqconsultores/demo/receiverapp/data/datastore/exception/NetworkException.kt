package com.peqconsultores.demo.receiverapp.data.datastore.exception

import java.lang.Exception

/**
 * Created by jesusabv93 on 18/04/2019.
 */
class NetworkException(message: String, ex: Exception) : Exception(message)