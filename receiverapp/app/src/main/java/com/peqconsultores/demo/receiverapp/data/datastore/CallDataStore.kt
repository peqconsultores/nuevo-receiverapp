package com.peqconsultores.demo.receiverapp.data.datastore

import com.peqconsultores.demo.receiverapp.data.datastore.exception.CallException
import com.peqconsultores.demo.receiverapp.data.datastore.exception.NetworkException
import com.peqconsultores.demo.receiverapp.data.datastore.exception.ParseException
import com.peqconsultores.demo.receiverapp.data.datastore.rest.models.CallResponse

/**
 * Created by jesusabv93 on 29/04/2019.
 */
interface CallDataStore {
    @Throws(CallException::class, NetworkException::class, ParseException::class)
    fun getCalls(origin: String): List<CallResponse>

    @Throws(CallException::class, NetworkException::class, ParseException::class)
    fun postCall(
        appOrigin: String,
        answered: Int,
        duration: Long,
        origin: String,
        destinationPhone: String,
        originPhone: String,
        versionAppOrigin: String,
        order: String,
        client: String,
        terminal: String,
        tel_origen_2: String?,
        oficina: String?,
        centro: String?
    ): Boolean
}