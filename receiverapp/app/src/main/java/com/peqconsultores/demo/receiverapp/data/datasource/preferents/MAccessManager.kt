package com.peqconsultores.demo.receiverapp.data.datasource.preferents

object MAccessManager {
    fun saveOrigin(spreferences: MSharedPreferences, word: String) {
        spreferences.saveOrigin(word)
    }
    fun saveDriverCode(spreferences: MSharedPreferences, word: String) {
        spreferences.saveDriverCode(word)
    }
    fun saveDestinationPhone(spreferences: MSharedPreferences, word: String) {
        spreferences.saveDestinationPhone(word)
    }
    fun saveSecondDestinationPhone(spreferences: MSharedPreferences, word: String) {
        spreferences.saveSecondDestinationPhone(word)
    }
    fun saveVersionAppOrigin(spreferences: MSharedPreferences, word: String) {
        spreferences.saveVersionAppOrigin(word)
    }
    fun saveAppOrigin(spreferences: MSharedPreferences, word: String) {
        spreferences.saveAppOrigin(word)
    }
    fun saveOrder(spreferences: MSharedPreferences, word: String) {
        spreferences.saveOrder(word)
    }
    fun saveClient(spreferences: MSharedPreferences, word: String) {
        spreferences.saveClient(word)
    }
    fun saveTerminal(spreferences: MSharedPreferences, word: String) {
        spreferences.saveTerminal(word)
    }
    fun saveOficinaSLL(spreferences: MSharedPreferences, word: String) {
        spreferences.saveOficinaSLL(word)
    }
    fun saveCentro(spreferences: MSharedPreferences, word: String) {
        spreferences.saveCentro(word)
    }
    fun saveFlag(spreferences: MSharedPreferences, word: String) {
        spreferences.saveFlag(word)
    }
}