package com.peqconsultores.demo.receiverapp.presentation.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

/**
 * Created by jesusabv93 on 28/04/2019.
 */
@Parcelize
data class CallReceiver(
    val origin: String,
    val driverCode: String,
    var destinationPhone: String,
    val appOrigin: String,
    val versionAppOrigin: String,
    val order: String,
    val client: String,
    var from: String,
    val terminal: String,
    var tel_origen_2: String?,
    var oficina: String?,
    var centro: String?
) : Parcelable